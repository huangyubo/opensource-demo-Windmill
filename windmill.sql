--创建windmill表
create table windmill(
  id int ,
  name varchar,
  age int
)

--向windmill表插入数据
insert into windmill values
(1,'a',18)
,(2,'b',19)
,(3,'c',19)
,(4,'d',20)
,(5,'e',19)
,(7,'f',21)
,(8,'g',22)
,(9,'h',23)
,(10,'i',24)

--查询windmill表
select * from windmill

--更新id为1的用户，名称改为ab

update windmill set name='ab'
where id=1

